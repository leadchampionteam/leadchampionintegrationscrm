package com.leadchampion.integrations;

import java.io.IOException;
import java.util.function.Function;

import okhttp3.Response;
import okhttp3.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


public class IntegrationsCrmResponse {
	private static final Logger logger = LogManager.getLogger(IntegrationsCrmResponse.class);
	private boolean success;
	private Integer responseCode;
	private Object responseBody;

	public IntegrationsCrmResponse(boolean success, Integer responseCode, Object body) {
		this.success = success;
		this.responseCode = responseCode;
		this.responseBody = body;
	}

	public IntegrationsCrmResponse(Response response) throws IOException, JSONException, IntegrationCrmException{
		this(response, null);
	}
	public IntegrationsCrmResponse(Response response, String fieldName) throws IOException, JSONException, IntegrationCrmException{
		this(response, fieldName, null);
	}

	public IntegrationsCrmResponse(Response response, String fieldName, Function<Object, Object> actionOnBody) throws IOException, JSONException, IntegrationCrmException{
		this.success = response.isSuccessful();
		this.responseCode = response.code();
		ResponseBody body = response.body();
		if (body == null) {
			this.responseBody = null;
			return;
		} else {
			this.responseBody = body.string();
		}
		if (!success) {
			throw new IntegrationCrmException("Chiamata al crm fallita! Code: "+responseCode+" Body: "+responseBody);
		}

		if (fieldName != null) {
			try {
				// se la risposta è un json object allora la gestisco come un JSONObject
				responseBody = new JSONObject(responseBody.toString());
			} catch (final JSONException ex) {
				// errore: è specificato un fieldLabel quindi deve essere un JSON Object
				throw  new IntegrationCrmException("Errore di parsing della risposta del CRM, il body non è un JSONObject: "+responseBody);
			}
			// provo con jsonObject
			try {
				// se la risposta è un json object allora la gestisco come un JSONObject
				responseBody = ((JSONObject) responseBody).getJSONObject(fieldName);
			} catch (final JSONException ex) {
				try {
					// se la risposta è un json array la gestisco come un JSONArray
					responseBody = ((JSONObject) responseBody).getJSONArray(fieldName);
				} catch (final JSONException ignoredArray) {
					// altrimenti rimane una stringa
					responseBody = ((JSONObject) responseBody).get(fieldName);
				}
			}
		} else {
			try {
				// se la risposta è un json object allora la gestisco come un JSONObject
				responseBody = new JSONObject(responseBody.toString());
			} catch (final JSONException ignoredObject){
				try {
					// se la risposta è un json array la gestisco come un JSONArray
					responseBody = new JSONArray(responseBody.toString());
				} catch (final JSONException ignoredArray) {
					// altrimenti rimane una stringa
				}
			}
		}
		if (actionOnBody != null) {
			if(responseBody instanceof JSONObject){
				actionOnBody.apply(responseBody);
			} else if (responseBody instanceof JSONArray){
				for (int i = 0; i < ((JSONArray) responseBody).length(); i++) {
					JSONObject item = ((JSONArray) responseBody).getJSONObject(i);
					actionOnBody.apply(item);
				}
			}
		}
	}

	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public Object getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(Object responseBody) {
		this.responseBody = responseBody;
	}

	@Override
	public String toString() {
		return toString(" | ");
	}
	public java.lang.String toString(String sep) {
		return "IntegrationsCrmResponse{" +
			sep + "success=" + success +
			sep + "responseCode=" + responseCode +
			sep + "responseBody='" + responseBody.toString() + '\'' +
			sep + '}'
		;
	}
	public void print() {
		print(true);
	}
	public void print(boolean expandJsonBody) {
		if (!success) {
			logger.error(String.format("*** RESPONSE ERROR: CODE=%d - MESSAGE=%s%n",responseCode,responseBody.toString()));
			return;
		}

		Object saveBody = responseBody;

		if (expandJsonBody) {
			try {
				// try conversion to JSONObject
				responseBody = (new JSONObject(saveBody.toString())).toString(4);
			} catch (Exception ex) {
				try {
					// try conversion to JSONArray
					JSONArray jsArray = new JSONArray(saveBody.toString());
					responseBody = "Number of items = " + jsArray.length() + "\n" + jsArray.toString(4);
				} catch (Exception ignored) {}
			}
		}
		// stampo l'oggetto con il responseBody eventualmente trasformato
		logger.info("\n*** RESPONSE OK ***\n"+toString("\n\t"));
		// riprisitno il responseBody
		responseBody = saveBody;
	}
}