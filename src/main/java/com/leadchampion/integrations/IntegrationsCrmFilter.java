package com.leadchampion.integrations;

public class IntegrationsCrmFilter {
	public enum CrmFilterOperator {
		CONTAINS,
		NOT_CONTAINS,
		EQUAL,
		NOT_EQUAL,
		GREATER_THAN,
		GREATER_THAN_OR_EQUAL,
		LOWER_THAN,
		LOWER_THAN_OR_EQUAL,
		HAS_PROPERTY,
		NOT_HAS_PROPERTY
	}

	private String fieldName;
	private CrmFilterOperator operator;
	private Object fieldValue;

	public String getFieldName() { return fieldName; }
	public void setFieldName(String fieldName) { this.fieldName = fieldName; }

	public CrmFilterOperator getOperator() { return operator; }
	public void setOperator(CrmFilterOperator operator) { this.operator = operator; }

	public Object getFieldValue() { return fieldValue; }
	public void setFieldValue(Object fieldValue) { this.fieldValue = fieldValue; }

	public IntegrationsCrmFilter() { }

	public IntegrationsCrmFilter(String fieldName, CrmFilterOperator operator, Object fieldValue) {
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.operator = operator;
	}
}