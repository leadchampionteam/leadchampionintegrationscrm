package com.leadchampion.integrations.tester;

import java.util.Scanner;
import org.apache.commons.lang3.StringUtils;


public class IntegrationsCrmTesterUtils {

	public static Integer readInt(String prompt, Integer min, Integer max, Integer defaultValue) {
		Scanner reader = new Scanner(System.in);
		while (true) {
			System.out.print(prompt+" (default="+defaultValue+") : ");
			try {
				String text = reader.nextLine();
				if (StringUtils.isEmpty(text)) {
					return defaultValue;
				}
				int number = Integer.parseInt(text);
				if (min != null
				&&  min > number) {
					throw new RuntimeException("Integer must be equals or greater than " + min);
				}
				if (max != null
				&&  max < number) {
					throw new RuntimeException("Integer must be equals or lower than " + max);
				}
				return number;
			} catch (Exception ex) {
				System.err.println("Choose an integer in the range "+min+" - "+max);
			}
		}
	}

	public static String readString(String prompt, Integer minLength, Integer maxLength) {
		Scanner reader = new Scanner(System.in);
		while (true) {
			System.out.print(prompt);
			String text = reader.nextLine();
			if (minLength != null
			&&  minLength > text.length()) {
				System.err.println("Text must be equals or longer than " + minLength+" chars");
				continue;
			}
			if (maxLength != null
			&&  maxLength < text.length()) {
				System.err.println("Text must be equals or shorter than " + maxLength+" chars");
				continue;
			}
			return text;
		}
	}
}
