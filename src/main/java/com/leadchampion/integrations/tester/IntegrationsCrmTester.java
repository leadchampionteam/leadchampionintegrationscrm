package com.leadchampion.integrations.tester;

import com.leadchampion.integrations.IntegrationsCrmResponse;
import com.leadchampion.integrations.IntegrationsCrmFilter;
import com.leadchampion.integrations.IntegrationsCrmFilters;
import com.leadchampion.integrations.IntegrationsCrmDAO;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class IntegrationsCrmTester {
	private static final Logger logger = LogManager.getLogger(IntegrationsCrmTester.class);
	public static IntegrationsCrmDAO crmDAO;
	public static String labelOfEntityName;


	/*************************************************
	 * USAGE:
	 *
	 *		public static void main(String[] args) {
	 *			new IntegrationsCrmTester(new crmDAOImplementation());
	 *		}
	 *
	 *************************************************/

	public IntegrationsCrmTester(IntegrationsCrmDAO crmDAOImpl) {
		this(crmDAOImpl, null);
	}

	public IntegrationsCrmTester(IntegrationsCrmDAO crmDAO, String labelOfEntityName) {
		IntegrationsCrmTester.labelOfEntityName = labelOfEntityName;
		IntegrationsCrmTester.crmDAO = crmDAO;

		while(true) {
			String menu = "\n" +
					"\n\t***************************" +
					"\n\tTester menu: "+
					"\n\t 1 - LIST Entities Names"+
					"\n\t 2 - COUNT Entity Items"+
					"\n\t 3 - COUNT ALL Entities Items"+
					"\n\t 4 - LIST ALL Entity Items"+
					"\n\t 5 - SEARCH Entity Items"+
					"\n\t 6 - CREATE Entity Item"+
					"\n\t 7 - GET Entity Item"+
					"\n\t 8 - DELETE Entity Item"+
					"\n\t 9 - UPDATE Entity Item"+
					"\n\t10 - REFRESH Token"+
					"\n\t0 - QUIT"+
					"\nChoose function number: ";
			int choice = IntegrationsCrmTesterUtils.readInt(menu,0,11,0);

			try {
				switch (choice) {
					case 1: entitiesList(true); break;
					case 2: countEntityItems(null); break;
					case 3: countAllEntitiesItems(); break;
					case 4: listEntityItems(); break;
					case 5: searchEntityItems(); break;
					case 6: createEntityItem(); break;
					case 7: getEntityItem(); break;
					case 8: deleteEntityItem(); break;
					case 9: updateEntityItem(); break;
					case 10: menuRefresh(); break;
					default:
						System.exit(0);
				}
			} catch (Exception ex){
				logger.error("ERROR", ex);
				ex.printStackTrace();
			}
		}
	}

	private static List<String> entitiesList(boolean doPrint) throws Exception {
		System.out.println("### ENTITIES LIST ###");

		IntegrationsCrmResponse response = crmDAO.getEntitiesList();
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in getEntitiesList: ["+response.getResponseCode()+"] "+response.getResponseBody().toString());
			return null;
		}

		JSONArray result = (JSONArray)response.getResponseBody();

		List<String> entitiesNames = new ArrayList<>();

		for (int i=0; i< result.length(); i++) {
			JSONObject entityJSON = result.getJSONObject(i);
			String entityName = entityJSON.getString((labelOfEntityName != null) ? labelOfEntityName : IntegrationsCrmDAO.CRM_ENTITY_NAME_FIELDNAME);
			entitiesNames.add(entityName);

			if (doPrint) {
				System.out.println(entityName);
			}
		}
		return entitiesNames;
	}

	public static void countEntityItems(String entityName) throws Exception {
		System.out.println("### COUNT ENTITY ITEMS ###");

		if (entityName == null) {
			entityName = IntegrationsCrmTesterUtils.readString("Choose the entity name:", 2, null);
		}

		IntegrationsCrmResponse response = crmDAO.countEntityItems(entityName);
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in countEntityItems: ["+response.getResponseCode()+"] "+response.getResponseBody().toString());
			return;
		}

		try {
			int count = Integer.parseInt(response.getResponseBody().toString());
			System.out.println("\tNumber of '"+entityName+"' entity items = "+count);
		} catch (Exception ex) {
			String errMsg = "Error parsing count response, not a Integer value:"+response.getResponseBody();
			System.err.println(errMsg);
		}
	}

	public static void countAllEntitiesItems() throws Exception {
		System.out.println("### COUNT ALL ENTITY ITEMS ###");

		List<String> entityNames = entitiesList(false);

		if (entityNames == null
		||  entityNames.size() == 0) {
			System.err.println("entitiesList is empty");
			return;
		}
		List<String> entities = entitiesList(false);
		if(entities == null || entities.isEmpty()){
			System.out.println("Nessuna entità trovata!");
			return;
		}
		for (String entityName : entities) {
			countEntityItems(entityName);
		}
	}

	private static void listEntityItems() throws Exception {
		System.out.println("### LIST ENTITY ITEMS ###");

		String entityName = IntegrationsCrmTesterUtils.readString("Choose the entity name:",2,null);

		IntegrationsCrmResponse response = crmDAO.getEntityItems(entityName,0,null);
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in countEntityItems: ["+response.getResponseCode()+"] "+response.getResponseBody().toString());
			return;
		}

		JSONArray entityItems = (JSONArray)response.getResponseBody();
		for (int i = 0; i < entityItems.length(); i++) {
			JSONObject entityItem = entityItems.getJSONObject(i);
			System.out.println(entityName+"["+i+"] = "+entityItem.toString());
		}
	}

	public static void searchEntityItems() throws Exception {
		System.out.println("### SEARCH ENTITY ITEMS ###");

		String entityName       = IntegrationsCrmTesterUtils.readString("Choose the entity name: ",2,null);
		String entityFieldName  = IntegrationsCrmTesterUtils.readString("Choose the entity field name: ",2,null);

		String operators = "";
		List<IntegrationsCrmFilter.CrmFilterOperator> operatorsList = new ArrayList();
		for (IntegrationsCrmFilter.CrmFilterOperator operator : IntegrationsCrmFilter.CrmFilterOperator.values()) {
			operatorsList.add(operator);
			operators += "\n\t["+operatorsList.size()+"] "+operator.toString();
		}
		int operatorIndex = IntegrationsCrmTesterUtils.readInt("COMPARE OPERATORS\n"+
																	operators+
																	"\nChoose the compare operator: ",1,operatorsList.size(),2);

		String entityFieldValue = IntegrationsCrmTesterUtils.readString("Choose the entity field value: ",1,null);

		IntegrationsCrmFilters filters = new IntegrationsCrmFilters();
		filters.addFilter(entityFieldName, operatorsList.get(operatorIndex-1), entityFieldValue);

		Integer offset = IntegrationsCrmTesterUtils.readInt("Choose result OFFSET ",0,null, 0);
		Integer count  = IntegrationsCrmTesterUtils.readInt("Choose result COUNT (null = ALL)",0,null, null);
		IntegrationsCrmResponse response = crmDAO.getEntityItems(entityName,filters,offset,count);
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in countEntityItems: ["+response.getResponseCode()+"] "+response.getResponseBody().toString());
			return;
		}

		JSONArray entityItems = (JSONArray)response.getResponseBody();

		System.out.println("\n\n*********************************\nNum match = " + entityItems.length());
		for (int i = 0; i < entityItems.length(); i++) {
			JSONObject entityItem = entityItems.getJSONObject(i);
			System.out.println(entityName+"["+i+"] = "+entityItem.toString());
		}
	}

	private static void menuRefresh() throws Exception {
		System.err.println("Not yet implemented");
//		TokenResponse tr = crmDAO.forceRefreshToken();
//		System.out.println("NEW TOKEN:\n\t"+tr);
	}


	private static void createEntityItem() throws Exception {
		System.out.println("### CREATE ENTITY ITEM ###");

		String entityName = IntegrationsCrmTesterUtils.readString("Choose the entity name: ",2,null);

		JSONObject entityItem = new JSONObject();

		while (true) {
			String entityFieldName  = IntegrationsCrmTesterUtils.readString("Choose the entity field name (empty to finish): ",0,null);
			if (StringUtils.isEmpty(entityFieldName)) {
				break;
			}
			String entityFieldValue  = IntegrationsCrmTesterUtils.readString("Choose the entity field value: ",0,null);
			entityItem.put(entityFieldName,entityFieldValue);
		}

		IntegrationsCrmResponse response = crmDAO.createEntityItem(entityName, entityItem,true);
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in countEntityItems: ["+response.getResponseCode()+"] "+response.getResponseBody().toString());
			return;
		}

		entityItem = (JSONObject) response.getResponseBody();
		System.out.println("Created entityItem:\n"+entityItem.toString(4));
	}

	private static void updateEntityItem() throws Exception {
		System.out.println("### CREATE ENTITY ITEM ###");

		String entityName = IntegrationsCrmTesterUtils.readString("Choose the entity name: ",2,null);

		String entityItemID = IntegrationsCrmTesterUtils.readString("Choose the entity ID: ",1,null);

		JSONObject entityItem = new JSONObject();

		while (true) {
			String entityFieldName  = IntegrationsCrmTesterUtils.readString("Choose the entity field name (empty to finish): ",0,null);
			if (StringUtils.isEmpty(entityFieldName)) {
				break;
			}
			String entityFieldValue  = IntegrationsCrmTesterUtils.readString("Choose the entity field value: ",0,null);
			entityItem.put(entityFieldName,entityFieldValue);
		}

		IntegrationsCrmResponse response = crmDAO.updateEntityItem(entityName, entityItemID, entityItem,true);
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in countEntityItems: ["+response.getResponseCode()+"] "+response.getResponseBody().toString());
			return;
		}

		entityItem = (JSONObject) response.getResponseBody();
		System.out.println("Updated entityItem:\n"+entityItem.toString(4));
	}

	private static void getEntityItem() throws Exception {
		System.out.println("### GET ENTITY ITEM ###");

		String entityName = IntegrationsCrmTesterUtils.readString("Choose the entity name: ",2,null);

		String entityItemId = IntegrationsCrmTesterUtils.readString("Choose the entity ID: ",1,null);

		IntegrationsCrmResponse response = crmDAO.getEntityItem(entityName,entityItemId);
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in countEntityItems: ["+response.getResponseCode()+"] "+response.getResponseBody().toString());
			return;
		}

		JSONObject entityItem = (JSONObject)response.getResponseBody();
		System.out.println("Retrieved entityItem:\n"+entityItem.toString(4));
	}

	private static void deleteEntityItem() throws Exception {
		System.out.println("### DELETE ENTITY ITEM ###");

		String entityName = IntegrationsCrmTesterUtils.readString("Choose the entity name: ",2,null);

		String entityItemId = IntegrationsCrmTesterUtils.readString("Choose the entity ID: ",1,null);

		IntegrationsCrmResponse response = crmDAO.deleteEntityItem(entityName,entityItemId,true);
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in countEntityItems: ["+response.getResponseCode()+"] "+response.getResponseBody().toString());
			return;
		}

		JSONObject entityItem = (JSONObject)response.getResponseBody();
		System.out.println("Deleted entityItem:\n"+entityItem.toString(4));
	}

	private static void createVisitAsCompanyNote() throws Exception {
		System.out.println("### CREATE VISIT AS COMPANY NOTE ###");

		String entityName = IntegrationsCrmTesterUtils.readString("Choose the entity name for companies: ",2,null);

		String entityItemId = IntegrationsCrmTesterUtils.readString("Choose the company (crm) ID: ",1,null);

		String note = IntegrationsCrmTesterUtils.readString("Insert the note: ",1,null);

		IntegrationsCrmResponse response = crmDAO.createVisitAsCompanyNote(entityName,entityItemId,note);
		response.print();

		if (!response.isSuccess()) {
			System.err.println("Error in createVisitAsCompanyNote: ["+response.getResponseCode()+"] "+response.getResponseBody());
			return;
		}

		JSONObject entityItem = (JSONObject)response.getResponseBody();
		System.out.println("Deleted entityItem:\n"+entityItem.toString(4));
	}
}