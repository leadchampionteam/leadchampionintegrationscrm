package com.leadchampion.integrations;

import org.codehaus.jettison.json.JSONObject;


public interface IntegrationsCrmDAO {
	String CRM_ENTITY_NAME_FIELDNAME = "entityName";

	/**
	 * returns the list of all the accessible CRM entities
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody contains a JSONArray with JSONObjects
	 *	with field IntegrationsCrmDAO.CRM_ENTITY_NAME_FIELDNAME containing the name
	 * 	of each accessible entity.
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse getEntitiesList() throws Exception;

	/**
	 * return the total number of items of the entity
	 * @param entityName
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody field contains the Integer
	 *	representing the number of items of the entity
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse countEntityItems(String entityName) throws Exception;

	/**
	 * return the items of the entity starting from "offset" for max count items
	 * @param entityName
	 * @param offset
	 * 	null value means default = 0
	 * @param count
	 * 	null value means no count limit
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody contains up to "count" items of the entity,
	 *	starting from the "offset"
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse getEntityItems(String entityName, Integer offset, Integer count) throws Exception;

	/**
	 * @param entityName
	 * @param filters
	 * 	filtering criteria in order to select a subset of items
	 * @param offset
	 * 	null value means default = 0
	 * @param count
	 * 	null value means no count limit
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody contains up to max "count" items of the entity matching criteria,
	 *	starting from the "offset"
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse getEntityItems(String entityName, IntegrationsCrmFilters filters, Integer offset, Integer count) throws Exception;

	/**
	 * retrieve the entity item identified by its "id"
	 * @param entityName
	 * @param entityItemID
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody contains the retrieved entity item, if found. Null otherwise.
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse getEntityItem(String entityName, String entityItemID) throws Exception;

	/**
	 * deletes the entity item identified by its "id"
	 * @param entityName
	 * @param entityItemID
	 * @param returnTheDeletedItem
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody contains the JSONObject of the deleted item if requested, or the CRM delete command response
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse deleteEntityItem(String entityName, String entityItemID, boolean returnTheDeletedItem) throws Exception;

	/**
	 * creates a new entity item
	 * @param entityName
	 * @param entityJson
	 * @param returnTheCreatedItem
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody contains the JSONObject of the created item if requested, or the CRM create command response
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse createEntityItem(String entityName, JSONObject entityJson, boolean returnTheCreatedItem) throws Exception;

	/**
	 * updates the entity item identified by its "id"
	 * @param entityName
	 * @param entityItemID
	 * @param entityJson
	 * @param returnTheUpdatedItem
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody contains the JSONObject of the updated item if requested, or the CRM update command response
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse updateEntityItem(String entityName, String entityItemID, JSONObject entityJson, boolean returnTheUpdatedItem) throws Exception;

	/**
	 * updates the entity item identified by its "id"
	 * @param crmCompanyId
	 * @param noteBody
	 * @param companyEntityName
	 * @return
	 * if IntegrationsCrmResponse.success == true :<br>
	 *	IntegrationsCrmResponse.responseBody is empty (status code 204)
	 * if IntegrationsCrmResponse.success == false :<br>
	 *	IntegrationsCrmResponse.responseBody contains the error data
	 * @throws Exception
	 */
	IntegrationsCrmResponse createVisitAsCompanyNote(String companyEntityName, String crmCompanyId, String noteBody) throws Exception;
}