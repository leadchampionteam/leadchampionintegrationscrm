package com.leadchampion.integrations;

import java.util.ArrayList;
import java.util.List;


public class IntegrationsCrmFilters {
	public enum CrmFiltersOperator {AND, OR}

	private CrmFiltersOperator booleanOperator;
	private List<IntegrationsCrmFilter> filters;

	public IntegrationsCrmFilters() {
		this(CrmFiltersOperator.AND);
	}

	public IntegrationsCrmFilters(CrmFiltersOperator booleanOperator) {
		this(booleanOperator,new ArrayList<>());
	}

	public IntegrationsCrmFilters(CrmFiltersOperator booleanOperator, List<IntegrationsCrmFilter> filters) {
		this.booleanOperator = booleanOperator;
		this.filters = filters;
	}

	public CrmFiltersOperator getBooleanOperator() { return booleanOperator; }
	public void setBooleanOperator(CrmFiltersOperator booleanOperator) { this.booleanOperator = booleanOperator; }

	public List<IntegrationsCrmFilter> getFilters() { return filters; }
	public void setFilters(List<IntegrationsCrmFilter> filters) { this.filters = filters; }

	public void addFilter(String fieldName, IntegrationsCrmFilter.CrmFilterOperator operator, Object fieldValue) {
		addFilter(new IntegrationsCrmFilter(fieldName,operator,fieldValue));
	}

	public void addFilter(IntegrationsCrmFilter filter) {
		filters.add(filter);
	}
}
