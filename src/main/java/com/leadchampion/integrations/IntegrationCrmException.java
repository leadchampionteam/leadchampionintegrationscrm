package com.leadchampion.integrations;

public class IntegrationCrmException extends Exception {

	public IntegrationCrmException() {
		super();
	}

	public IntegrationCrmException(String message) {
		super(message);
	}
}
